import os
import re
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from skimage import exposure, measure


def get_output_basename(seg_output_datadir, sample, img_path):
    """
    obtains basename of individual nuclei e.g. n11_002_E6--W00054--P00001
    """
    img_path_basename = os.path.basename(img_path)   
    match = re.search(".*(?=--Z00000)", img_path_basename) 
    output_basename = match.group(0) 
    
    return output_basename


def get_img_patch(img, df, idx):
    """
    obtains the image patch containing the segmented nucleus, defined by the coordinates of the bounding box.
    """
    minr, minc, maxr, maxc = df.loc[idx, ['min_row', 'min_col', 'max_row', 'max_col']].astype('int64')
    img_patch = img[minr:maxr, minc:maxc]         
    
    return img_patch


def get_cent_intensity(img, df, idx):
    """
    obtains the mean intensity for a 10x10 pixel square centred on the image centroid
    """
    cent_row, cent_col = df.loc[idx, ['centroid_row', 'centroid_col']].astype('int64')
    cent_patch = img[cent_row-5:cent_row+5, cent_col-5:cent_col+5]
    cent_intensity = cent_patch.mean()    
    
    return cent_intensity


def convert_to_3channel(img_blue, img_red): 
    """
    takes in two separate images (of depth 1) containing the 'blue' and 'red' channels,
    merges them into a single image (of depth 3) containing both channels
    """
    img_null = np.zeros(img_blue.shape, dtype=img_blue.dtype)                      
    img_merged = np.dstack((img_red/img_red.max(), img_null, img_blue/img_blue.max()))
    
    return img_merged


def format_image(img):
    """
    converts image to uint8 (before saving)
    """
    img_formatted = (img*255 / np.max(img)).astype('uint8')
    im = Image.fromarray(img_formatted)
    
    return im


def exposure_is_low(image, threshold):
    """
    returns True if exposure below threshold
    """
    return exposure.is_low_contrast(image, fraction_threshold=threshold)


def get_blur(image):
    """
    compute a metric that indicates the strength of blur in an image
    higher values correspond to decreased sharpness
    """
    return measure.blur_effect(image)   


def get_sharpness(image):
    """
    computes average gradient magnitude,
    higher values correspond to increased sharpness
    """
    gy, gx = np.gradient(image)
    gnorm = np.sqrt(gx**2 + gy**2)
    sharpness = np.average(gnorm)   
    
    return sharpness


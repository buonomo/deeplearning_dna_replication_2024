import os

try:
    import segmentation
except:
    print("WARNING: please follow instructions main README.md to setup this repository as a python package")


def find_root_directory():
    """
    obtain path to root directory of project
    """
    root_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    
    return root_path


def find_data_directory():
    """
    obtain path to data directory containing images
    """
    root_path = find_root_directory()
    data_path = os.path.join(root_path, 'images')
    
    return data_path


def find_files_in_directory(dir_path, file_ending):
    """ 
    finds all the files in dir_path that end in file_ending, (searching recursively through subdirectories)
    returns: list of their complete file paths.
    """
    file_paths = []
    for root, subdirs, files in os.walk(dir_path):
        
        for file in files:
            if file.endswith(file_ending):
                file_paths.append(os.path.join(root, file))
                
    return file_paths


def filter_files(files_list, search_filter):
    """ 
    filters for specific files for those matching search filter
    files_list: files to be filtered
    search_filter: phrase to search for (e.g 'n14_001' or 'n11_001_E8--W00056--P00001--Z00000--T00000--CY5.tif'
    returns: files that match the search filter
    """
    return [file 
            for file in files_list
            if search_filter in file]


def establish_folder(directory, foldername):
    """
    returns path to a foldername in a directory, creating the folder if it does not exist.
    """
    path = os.path.join(find_data_directory(), directory, foldername)
    try:
        os.mkdir(path)
        print(f"created folder: {path}")
    except FileExistsError:
        pass
    return path


def establish_datadir_folder(foldername):
    """
    returns path to a folder in the user data directory, creating the folder if it does not exist.
    """
    return establish_folder(find_data_directory(), foldername)


def is_folder_empty(folderpath):
    """
    returns true if a folder path is empty, false otherwise
    """
    return len(os.listdir(folderpath)) == 0


def establish_segmentation_output_folders(folderpath, lst):
    """
    creates folders for each of the items/samples in the list
    returns the paths to each folder used to store segmented data,
    creating the folder paths in the os directory if necessary 
    (if the data hasn't already been generated)
    """ 
    seg_output_datadir = establish_datadir_folder(folderpath)
    for output_folder in lst:
        establish_folder(seg_output_datadir, output_folder) 

    return seg_output_datadir
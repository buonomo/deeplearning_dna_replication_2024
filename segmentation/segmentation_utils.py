import numpy as np
import pandas as pd
from scipy import ndimage as ndi
from skimage import feature, filters, measure, morphology, segmentation

def thresh_otsu(image):
    """
    segments image by thresholding using Otsu's method, then applying the watershed algorithm to obtain individual nuclei.
    
    References:
    1) https://scikit-image.org/docs/stable/auto_examples/segmentation/plot_label.html#sphx-glr-auto-examples-segmentation-plot-label-py
    2: https://scikit-image.org/docs/stable/auto_examples/segmentation/plot_watershed.html#sphx-glr-auto-examples-segmentation-plot-watershed-py
    """
    
    # apply otsu threshold
    thresh = filters.threshold_otsu(image)
    nuclei = image > thresh

    # fill holes
    nuclei = morphology.remove_small_holes(nuclei, area_threshold=100)

    # remove artifacts connected to image border
    nuclei = segmentation.clear_border(nuclei)

    # plt.imshow(nuclei, cmap='gray')   
    distance = ndi.distance_transform_edt(nuclei)

    # Adjust min_distance accordingly
    local_max_coords = feature.peak_local_max(distance, min_distance=20, labels=nuclei)
    local_max_mask = np.zeros(distance.shape, dtype=bool)
    local_max_mask[tuple(local_max_coords.T)] = True
    markers = measure.label(local_max_mask)

    segmented_nuclei = segmentation.watershed(-distance, markers, mask=nuclei)
    label_nuclei = measure.label(segmented_nuclei)

    return label_nuclei

    
def get_regionprops_dataframe(label_nuclei, intensity_image, cell_type, conditions_list):
    """
    computes image properties of segmented nuclei and returns them as a pandas dataframe,
    then filters the dataframe according to a list of conditions 
    """
    
    properties_list = ('label', 'area', 'major_axis_length', 'minor_axis_length', 'eccentricity', 'centroid', 'bbox', 'intensity_mean')
    columns_list = ['label', 'area', 'major_axis_length', 'minor_axis_length', 'eccentricity', 'centroid_row', 'centroid_col', 'min_row', 'min_col', 'max_row', 'max_col', 'intensity_mean']  
    
    if cell_type == 'mESC':
        conditions_list = min_area, max_area, min_minor_axis_length, max_major_axis_length, max_eccentricity \
                        = 2000, 10000, 40, 120, 0.7     
    elif cell_type == 'u2os':
        conditions_list = min_area, max_area, min_minor_axis_length, max_major_axis_length, max_eccentricity \
                        = 2000, 25000, 40, 250, 0.8  
    
    individual_nuclei = measure.regionprops_table(label_nuclei, intensity_image=intensity_image, properties=properties_list)
    df = pd.DataFrame(individual_nuclei)
    df.columns = columns_list
    df['intensity_total'] = df['intensity_mean'] * df['area']
    
    min_area, max_area, min_minor_axis_length, max_major_axis_length, max_eccentricity = conditions_list 
    condition = (df['area'] > min_area) & \
                (df['area'] < max_area) & \
                (df['minor_axis_length'] > min_minor_axis_length) & \
                (df['major_axis_length'] < max_major_axis_length) & \
                (df['eccentricity'] < max_eccentricity)
    df = df[condition].reset_index(drop=True)

    return df

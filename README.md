# Deep learning for studying DNA replication spatiotemporal dynamics

This repository contains the code associated with the following paper: [Supervised and unsupervised deep learning-based approaches for studying DNA replication spatiotemporal dynamics (Ng-Kee-Kwong et al., 2025)](https://www.nature.com/articles/s42003-025-07744-2)

## Installation and setup

Run the following commands on the terminal:

- Clone this repository on your local machine

`git clone https://git.ecdf.ed.ac.uk/buonomo/deeplearning_dna_replication_2024.git`

- Create conda environment with required dependencies

`conda env create -f environment.yml`

- Install this repository as a python package

`pip install -e .`


## Repository organisation

- `segmentation`: Directory containing code for running segmentation and associated utility functions.

- `ml`: Directory containing code for training deep learning models, generating embeddings and performing analyses and visualisation.

- `annotation`: Directory containing list of segmented nuclei and relevant annotation.

- `images`: Directory containing image datasets (publicly available on Edinburgh DataShare: https://doi.org/10.7488/ds/7754). 


## Instructions

Run the following scripts in the order presented below:

- 'run_segmentation_mESC.ipynb' and 'run_segmentation_u2os.ipynb' contain the code for segmenting images and filtering them for individual S-phase nuclei.

- 'run_classification_train_test.ipynb' and 'run_BYOL_train.ipynb' contain the code for training and testing the model on the segmented image datasets, and will save the trained 'supervised' and 'unsupervised' models, respectively. 

- (Optional) 'run_classification_test_only.ipynb' contains the code for testing the trained supervised model model on a different segmented image dataset (without fine-tuning).

- (Optional) 'run_classification_small_models.ipynb' and 'run_classification_boyd_model.ipynb' contain code similar to 'run_classification_test_only.ipynb', but using smaller neural network architectures. 

- 'run_classification_predict_only.ipynb' will use the trained supervised model to predict the S-phase category of all segmented nuclei in the mESC image dataset.  

- 'generate_embeddings.ipynb' will use the trained supervised or unsupervised model to generate embeddings for S-phase nuclei in the entire image dataset.

- The remaining scripts (all starting with 'analysis') can then be used to perform the different analyses and visualisation once the embeddings have been generated.


## Reproducing figures

| Figure  | Notebook |
| ------- | ------- |
| Fig 1a.       | N/A |
| Fig 1b.       | analysis_distribution.ipynb |
| Fig 1c.       | run_classification_train_test.ipynb |
| Fig 2a-2c.    | analysis_visualisation_supervised_mESC.ipynb |
| Fig 3a.       | analysis_distribution.ipynb |
| Fig 3b.       | analysis_visualisation_supervised_mESC.ipynb |
| Fig 4a-4b.    | analysis_visualisation_unsupervised_mESC.ipynb |
| Fig 4c-4d.    | analysis_quantification_mESC.ipynb |
| Fig 5a-5b.    | analysis_visualisation_unsupervised_u2os.ipynb |
| Fig 5c-5e.    | analysis_quantification_u2os.ipynb |
| Supplementary Fig 1a.         | run_classification_test_only.ipynb |
| Supplementary Fig 1b.         | analysis_distribution.ipynb |
| Supplementary Fig 2a-2d.      | run_classification_small_models.ipynb |
| Supplementary Fig 2e.         | run_classification_boyd_model.ipynb |
| Supplementary Fig 3.          | analysis_distribution.ipynb |
| Supplementary Fig 4a-4b.      | analysis_visualisation_supervised_mESC.ipynb |
| Supplementary Fig 5a.         | N/A |
| Supplementary Fig 5b.         | analysis_distribution.ipynb |
| Supplementary Fig 6a-6b.      | analysis_visualisation_unsupervised_mESC.ipynb |
| Supplementary Fig 7a-7b.      | N/A |
| Supplementary Fig 7c.         | analysis_distribution.ipynb |
| Supplementary Fig 8a-8b.      | N/A |
| Supplementary Fig 9a-9b.      | analysis_visualisation_unsupervised_u2os.ipynb |
| Supplementary Fig 10a-10b.    | analysis_visualisation_unsupervised_u2os.ipynb |
| Supplementary Fig 10c.        | analysis_quantification_u2os.ipynb |
| Supplementary Fig 11a.        | N/A |
| Supplementary Fig 11b.        | run_classification_train_test.ipynb |
| Supplementary Fig 11c.        | N/A |

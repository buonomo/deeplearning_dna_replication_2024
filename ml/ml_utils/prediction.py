import os
import numpy as np
import pandas as pd
import scipy.stats
import torch
from PIL import Image
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from collections import defaultdict


def get_predictions_and_targets(dataloader, idx, model, device, display=False):
    preds, targets = [], []    
    model.eval()
    with torch.no_grad():
        for X, y in dataloader:
            X, y = X.to(device), y.to(device)
            X = X.float()
            
            pred = model(X)
            y = y.long()
            predicted, actual = pred.argmax(1), y
                
            preds.extend(predicted)
            targets.extend(actual)
            
        preds = [pred.item() for pred in preds]
        targets = [target.item() for target in targets]
            
        if display:   
            for i, j in enumerate(idx): 
                print(f'Nucleus idx: {j} \tActual: {targets[i]}\t Predicted: {preds[i]}') 

    return preds, targets
    
    
def get_confusion_dataframe(preds, targets, show_values='proportion'):
    """
    computes the comfusion matrix data, telling us for each target class,
    the proportion of times the model predicts each target class.
    """
        
    # get possible classes (and their labels)
    confusion_data = defaultdict(list)
    target_classes = np.unique(targets)  
    preds, targets = np.array(preds), np.array(targets)

    for target_class in target_classes:
        # find targets per class
        target_ids = np.where(targets == target_class)
        preds_at_target = np.array([preds[i] for i in target_ids[0]])
        num_targets = len(target_ids[0])

        for c in target_classes:
            # find proportion of times each class was predicted, for the targets
            num_preds = np.sum(preds_at_target == c)
            proportion = num_preds / num_targets

            if show_values == 'exact':
                confusion_data[target_class].append(num_preds)
            elif show_values == 'proportion':
                confusion_data[target_class].append(proportion)  

    # convert to data frame
    df = pd.DataFrame(confusion_data)    
    return df
        

def plot_confusion_matrix(df, figsize=(7,6), cmap=plt.cm.Blues, title="", level='sphase'): 
       
    if level == 'sphase':
        class_labels = ['early', 'early-mid', 'mid', 'mid-late', 'late']
    elif level == 'overall':
        class_labels = ['S-phase', 'G1/G2-phase', 'Poor morphology/\nInaccurate segmentation', 'Blurry']
                        
    sns.color_palette("light:#5A9", as_cmap=True)

    plt.figure(figsize=figsize)
    p = sns.heatmap(df.round(3), cmap=cmap, annot=True, square=True, fmt='g')     # 3dp
    p.set_xlabel("Target")
    p.set_ylabel("Prediction")
    p.set_xticklabels(class_labels)
    p.set_xticklabels(p.get_xticklabels(),rotation = 0)
    p.set_yticklabels(class_labels)
    p.set_yticklabels(p.get_yticklabels(),rotation = 0)
    plt.xticks(fontsize=8)
    plt.yticks(fontsize=8)
    p.set_title(title)
    plt.tight_layout()
    plt.show()
    
        
def get_confidence_interval(test_accuracy, test_size, confidence=0.95):
    
    z_value = abs(scipy.stats.norm.ppf((1 - confidence) / 2.0))
    ci_length = z_value * np.sqrt((test_accuracy * (1 - test_accuracy)) / test_size)
    ci_lower = test_accuracy - ci_length
    ci_upper = test_accuracy + ci_length
    
    print(f"Test accuracy:{(100*test_accuracy):>0.1f}% +/- {(100*ci_length):>0.1f}%")
    print(f"Lower 95% CI: {(100*ci_lower):>0.1f}%")
    print(f"Upper 95% CI: {(100*ci_upper):>0.1f}%")      
          
    return ci_length, ci_lower, ci_upper
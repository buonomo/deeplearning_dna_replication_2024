import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader

class EarlyStopper:
    def __init__(self, minimum_firstsave_epoch=1, max_save_frequency=1, patience=50, min_delta=0):
        self.minimum_firstsave_epoch = minimum_firstsave_epoch - 1 # Subtract one to account for zero indexing 
        self.max_save_frequency = max_save_frequency - 1           # Same logic as above   
        self.patience = patience
        self.min_delta = min_delta
        
        self.counter = 0
        self.min_validation_loss = np.inf  
        self.max_validation_accuracy = 0 
        self.epochs_since_save = 0
        self.best_epoch = 0 # the best epoch for validation score seen by the model.
        self.cached_epoch = 0 # the last epoch at which the model weights were cached to disk
        self.stopped_early = False # set to true if the model stops training early as the early stopping threshold has been reached.       
        
    def early_stop(self, model, epoch, validation_loss, validation_accuracy, metric='accuracy', save_name= 'model_weights_temp.pth'):
        
        if metric == 'accuracy':
            # if accuracy is the same but validation loss decreases, save weights
            better_performance = ((validation_accuracy > self.max_validation_accuracy)) or \
                                 ((np.isclose(validation_accuracy, self.max_validation_accuracy)) and \
                                  (validation_loss < self.min_validation_loss)) 
            
        elif metric == 'loss':
            better_performance = (validation_loss < self.min_validation_loss)
                
        if better_performance:
            self.max_validation_accuracy = validation_accuracy
            self.min_validation_loss = validation_loss
            self.best_epoch = epoch
            self.counter = 0
                       
            if epoch > self.minimum_firstsave_epoch and self.epochs_since_save >= self.max_save_frequency: 
                self._cache_weights(model, epoch, save_name) # keep a cache of the best weights
                self.epochs_since_save = 0
                print("Cached model weights")
            else:
                self.epochs_since_save += 1
  
        else:
            self.counter += 1
            if self.counter >= self.patience:
                self.stopped_early = True
                return True
        return False
    
    def _cache_weights(self, model, epoch, save_name):
        """
        store a copy of the model's current weights
        """
        self.cached_epoch = epoch
        torch.save(model.state_dict(), save_name)    
    
    def restore_cached_weights(self, model, save_name):
        """
        replace the model weights with the wieghts stored in the cache
        """
        try:
            model.load_state_dict(torch.load(save_name))
        except FileNotFoundError:
            print("no model weights previously saved, saving current weights")
            self._cache_weights()

    def get_earlystopping_epochs(self):
        return {"best epoch":self.best_epoch, "cached epoch":self.cached_epoch, "stopped early": self.stopped_early}
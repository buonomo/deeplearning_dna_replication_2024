import numpy as np
from sklearn.model_selection import train_test_split
import torch
from torch import Tensor
from torch.utils.data import Dataset, DataLoader, random_split, Subset
import matplotlib.pyplot as plt

                           
def train_test_split_stratify(dataset, stratify, test_frac=0.2, shuffle=True, random_state=42):

    train_idx, test_idx = train_test_split(np.arange(len(dataset)),
                                             test_size=test_frac,
                                             random_state=random_state,
                                             shuffle=shuffle,
                                             stratify=stratify)
    
    train_dataset = Subset(dataset, train_idx)
    test_dataset = Subset(dataset, test_idx)
    
    return train_idx, test_idx, train_dataset, test_dataset


def train(dataloader, model, loss_fn, optimizer, scheduler, epoch, device):
    size = len(dataloader.dataset)
    model.train()
    total_training_loss = 0
    for batch, (X, y) in enumerate(dataloader):
        X, y = X.to(device), y.to(device)
        X = X.float()
        batch_size = len(X)
        
        # Compute prediction error
        pred = model(X)
        y = y.long()
        loss = loss_fn(pred, y)

        # Backpropagation
        loss.backward()
        optimizer.step()
        scheduler.step(epoch + batch / size)
        optimizer.zero_grad()

        loss, current = loss.item(), (batch + 1) * len(X)
        batch_loss = loss * batch_size
        total_training_loss += batch_loss
     
    avg_training_loss = total_training_loss / size
    print(f"Training Loss: {avg_training_loss:>8f}") 
    return avg_training_loss

    
def test(dataloader, model, loss_fn, device):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    model.eval()
    total_val_loss, correct = 0, 0
    with torch.no_grad():
        for X, y in dataloader:
            X, y = X.to(device), y.to(device)
            X = X.float()
            batch_size = len(X)
            
            # Compute prediction error
            pred = model(X)
            y = y.long()
            loss = loss_fn(pred, y).item()
                         
            batch_loss = loss * batch_size
            total_val_loss += batch_loss
            batch_correct = get_correct(pred, y)
            correct += batch_correct
               
    avg_val_loss = total_val_loss / size
    correct /= size
    print(f"Validation Loss: {avg_val_loss:>8f}\nAccuracy: {(100*correct):>0.1f}%\n")   
    return avg_val_loss, correct


def train_BYOL(dataloader, model, learner, optimizer, epoch, device):
    size = len(dataloader.dataset)
    model.train()
    total_training_loss = 0
    for batch, (X, y) in enumerate(dataloader):
        X, y = X.to(device), y.to(device)
        X = X.float() # Use this instead of converting model to double
        batch_size = len(X)
        
        # Compute loss
        loss = learner(X)

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        learner.update_moving_average() # update moving average of target encoder          
        
        loss, current = loss.item(), (batch + 1) * len(X)
        batch_loss = loss * batch_size
        total_training_loss += batch_loss
     
    avg_training_loss = total_training_loss / size
    print(f"Training Loss: {avg_training_loss:>8f}\n") 
    return avg_training_loss


def get_correct(pred, y):
    
    batch_correct = (pred.argmax(1) == y).type(torch.float).sum().item()
    
    return batch_correct


def plot_loss(train_losses, validation_losses):
    plt.figure(figsize=(10,5))
    plt.title("Training and Validation Loss")
    plt.plot(validation_losses,label="validation")
    plt.plot(train_losses,label="train")
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.legend()
    plt.show()
    
    
def plot_accuracy(validation_accuracy):
    plt.figure(figsize=(10,5))
    plt.title("Validation Accuracy")
    plt.plot(validation_accuracy,label="validation")
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")
    plt.legend()
    plt.show()


from torchvision import transforms

def get_transform_prenorm(img_size):
    """
    transformation applied pre-normalisation:
    converts image to tensor(float) and resizes to required image size
    """
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.ConvertImageDtype(float),
        transforms.Resize((img_size, img_size))
    ])
    
    return transform
    
    
def get_transform_train(mean, std):
    """
    transformation applied to training dataset:
    normalises images based on provided mean and standard deviation,
    then applies image augmentation (horizontal and vertical flips)
    """       
    transform = transforms.Compose([
        transforms.Normalize(mean=mean, std=std),
        transforms.RandomHorizontalFlip(p=0.5),   
        transforms.RandomVerticalFlip(p=0.5) 
    ])
        
    return transform


def get_transform_test(mean, std):
    """
    transformation applied to test dataset:
    normalises images based on provided mean and standard deviation,
    does not apply image augmentation to test dataset
    """         
    transform = transforms.Compose([
        transforms.Normalize(mean=mean, std=std)  
    ])
        
    return transform


def get_transform_predict(img_size, mean, std):
    """
    transformation applied to test dataset for prediction only:
    converts image to tensor(float) and resizes to required image size,
    before applying normalisation based on provided mean and standard deviation
    """

    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.ConvertImageDtype(float),
        transforms.Resize((img_size, img_size)),
        transforms.Normalize(mean=mean, std=std) 
    ])
    
    return transform

import os
import numpy as np
import pandas as pd
from PIL import Image
import torch
from torch.utils.data import Dataset


class CustomImageDataset(Dataset):
    def __init__(self, level, annotations_file, transform=None, target_transform=None):
        self.level = level
        self.img_labels = pd.read_csv(annotations_file, sep='\t') 
            
        self.transform = transform
        self.target_transform = target_transform

    def __len__(self):
        return len(self.img_labels)

    def __getitem__(self, idx):
        img_path = self.img_labels.loc[idx, 'img']
        image = Image.open(img_path)
        label = self.img_labels.loc[idx, self.level]

        if self.transform:
            image = self.transform(image)
        if self.target_transform:
            label = self.target_transform(label)
        return image, label
    
    
class DatasetFromSubset(Dataset):
    def __init__(self, subset, transform=None):
        self.subset = subset
        self.transform = transform
        
    def __len__(self):
        return len(self.subset)
    
    def __getitem__(self, idx):
        image, label = self.subset[idx]
        if self.transform:
            image = self.transform(image)
        return image, label
       
    

import os
import random
import numpy as np
import torch
from torch.utils.data import WeightedRandomSampler


def set_seeds(seed=42):
    """
    Sets random sets for torch operations.
    Args: seed (int, optional): Random seed to set (Defaults to 42)
    """
    
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)     # Set the seed for CUDA torch operations (ones that happen on the GPU)
    torch.cuda.manual_seed_all(seed) 
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True
    torch.use_deterministic_algorithms(True)
    os.environ['PYTHONHASHSEED'] = str(seed)
    os.environ["CUBLAS_WORKSPACE_CONFIG"]=":4096:8"    # Set environmental variable for deterministic behaviour
    

def compute_norm_params(dataset, modify):
    """
    dataset: a torch dataset object
    returns a dictionary of the mean and standard deviation of the dataset
    """
    # creates a [c, h, w, n] stack meaning channels (=3), height (e.g=224), width (=h) , n meaning number of images
    imgs = torch.stack([img_t for img_t, _,  in dataset], dim=3)
    
    # num channels
    c = dataset[0][0].shape[0]
    
    # mean and std per channel
    mean = imgs.view(c, -1).mean(dim=1)
    std = imgs.view(c, -1).std(dim=1)
    
    if modify == True:
        if std[std == 0].nelement() != 0:
            std[std == 0] = 1
            print("Changed std values from '0' to '1' to avoid division by zero when normalising")
    
    print("mean:", mean)
    print("std:", std)
    return mean, std


def get_weighted_sampler(dataset, label_list, return_weights=False, seed=42):
    """
    Sample images from the training set with sampling weights inversely proportional to the frequency of the class       they belong to, i.e. images from more frequently occurring classes are sampled less often.
    """
    label_unique, label_counts = np.unique(label_list, return_counts=True)
    label_weights = [sum(label_counts) / count for count in label_counts]
    label_weights_dict = dict(zip(label_unique, label_weights))
    label_weights_list = [label_weights[label] for label in label_list]
    
    sampler = WeightedRandomSampler(
        weights=label_weights_list, 
        num_samples=len(dataset), 
        replacement=True,
        generator=torch.Generator().manual_seed(seed)
    )
    
    if return_weights:
        print(label_weights_dict)
        return sampler, label_weights_dict
    else:
        return sampler
    
